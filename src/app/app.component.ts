import { Component, HostListener, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinct } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public fixedMenu = false;
  private pageByScroll$ = fromEvent(window, 'scroll')
    .pipe(
      map(() => window.scrollY),
      filter(current => current >=  document.body.clientHeight - window.innerHeight)
    );

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    const number = event.pageY;
    if (number > 50) {
      this.fixedMenu = true;
    } else {
      this.fixedMenu = false;
    }
  }

  constructor() {
    this.pageByScroll$.subscribe(val => {
      if (val > 50) {
        this.fixedMenu = true;
      } else {
        this.fixedMenu = false;
      }
    });
  }
  ngOnInit() {}
}
